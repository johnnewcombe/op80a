
/*  This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "OP80a.h"

void setup() {

  Serial.begin(9600, SERIAL_8N1);  
  delay(500);
  
  //define inputs (Data and RD) see OP80a.h
  for (int i = 0;i<9;i++) {
    pinMode(i+D0, INPUT); //OA80a D0-D7 are mapped to Nano D2 - D9 (see OP80a.h)
  }
  // verbose switch
  pinMode(A0, INPUT_PULLUP);
    
  // define outputs (ACK,S1 and S2) see OP80a.h
  for (int i = 0;i<3;i++) {
    pinMode(i+ACK, OUTPUT);
  }

}

void loop() {

   // set debug flag based on A0 being LOW
   verbose = !digitalRead(A0);

  // reset ACK and S2
  digitalWrite(ACK, LOW);
  digitalWrite(S2, LOW);  

  // loop looking for RD = HIGH
  while(!digitalRead(RD)) {
  }

  // RD High so read the data
  byte data = 0;

  // loop through, msb first
  for (int i = 7; i >= 0; i--) {

    // read the value 
    int pinBit = digitalRead(i+D0);
    
    //shift it to set the appropriate bit
    pinBit = pinBit << (i);

    // add the bit to the byte
    data = data | pinBit;
  }

  // signal Data with S1
  if(data > 0) {  
      digitalWrite(S2, HIGH);
  } else {
      digitalWrite(S2, LOW);    
  }

  // if debug write the serial monitor data
  if(verbose) {
    
    // S1 indicates Debug Mode
    digitalWrite(S1, HIGH);
    printVerbose(data);

  } else {

    // S1 indicates Debug Mode
    digitalWrite(S1, LOW);

    // else send data to serial port
    Serial.write(data);

  }

  // send an ACK
  digitalWrite(ACK, HIGH);
  
}
