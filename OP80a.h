

/*  This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef _OP80a_h
#define _OP80a_h

#include <Arduino.h>

// Pin assignments for Arduino Nano
// Note that data  pins are consecutive

// Inputs
const int D0 = 2;   // nano D2  (OP80a pin 1) 
const int D1 = 3;   // nano D3  (OP80a pin 16)
const int D2 = 4;   // nano D4  (OP80a pin 2)
const int D3 = 5;   // nano D5  (OP80a pin 15)
const int D4 = 6;   // nano D6  (OP80a pin 3)
const int D5 = 7;   // nano D7  (OP80a pin 14)
const int D6 = 8;   // nano D8  (OP80a pin 4)
const int D7 = 9;   // nano D9  (OP80a pin 13)
const int RD = 10;  // nano D10 (OP80a pin 7)

//Outputs
const int ACK = 11; // nano D11 (OP8a pin 5)
const int S2 = 12;  // nano D12 (OP8a pin 10)
const int S1 = 13;  // nano D13 (OP8a pin 11)


/* 
 *  These functions are used for the verbose output. 
 *  They are only used when A0 is connected to ground.
 */

int verbose;

void printHex(int num, int precision) {
  char tmp[16];
  char format[128];
  sprintf(format, "%%.%dX", precision);
  sprintf(tmp, format, num);
  Serial.print(tmp);
}

void printAscii(int num) {

  // tidy up non-printing chars for the serial monitor
  num = num & 0x7F;
  if(num < 0x20)
    num = '.';

  Serial.print("'");
  Serial.print(char(num));
  Serial.print("'");
}

void printBinary(int num) {
  
  for (int i = 0; i < 8; i++)
  {    
     if (num < pow(2, i)) {
           Serial.print(B0);
     }
  }
  Serial.print(num, BIN);
}

void printVerbose(int num) {
  
   printHex(num, 2);
   Serial.print(" ");
   printAscii(num);
   Serial.print(" ");
   printBinary(num);
   Serial.print("\r\n");
}

#endif


